app.controller('homeController', function ($scope) {

    $scope.pageTitle = "Dublin Bikes App";
})

app.controller('stationsController', function ($scope, $http) {

    $http.get("https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=923b9f8762f3bacf246b898bf50e66550436b145", {}).success(function (data) {

        $scope.stations = data;


    }).error(function () { alert('An error has occured') });

})

app.controller('stationController', function ($scope, $routeParams, $http) {

    $scope.pageTitle = "Dublin Bike Station for";
    $scope.stationId = $routeParams.id;

    $http.get("https://api.jcdecaux.com/vls/v1/stations/"+ $routeParams.id +"?contract=dublin&apiKey=923b9f8762f3bacf246b898bf50e66550436b145", {}).success(function (stationObject) {

        $scope.station = stationObject;

        console.log(stationObject);


    }).error(function () { alert('An error has occured') });



})
