var app = angular.module("dublinBikeApp", ['ngRoute'])


//Inject route provider
//Set up urls with a default url (otherwise)
//Set a template and controller for each url 

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/home', {
        templateUrl: 'templates/home.html',
        controller: 'homeController'
    }).
              when('/stations', {
        templateUrl: 'templates/stations.html',
        controller: 'stationsController'
    }).

    when('/station/:id', {
      templateUrl: 'templates/station.html',
      controller: 'stationController'
  }).
    
      
   
      otherwise({ //if we don't match urls, this is where we go
        redirectTo: '/home'
      });
}]);